﻿using analitfarm_kassir.model;
using analitfarm_kassir.Model;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace analitfarm_kassir
{
    /// <summary>
    /// Страница для отображения закрытых чеков
    /// </summary>
    public partial class ChecksPage : Page
    {
        public ChecksPage()
        {
            InitializeComponent();
            Refresh();
        }

        /// <summary>
        /// Обновление данных по закрытым чекам
        /// </summary>
        public void Refresh()
        {
            var db = new LocalDbContext();
            var checks = db.Checks.OrderBy(x => x.Data).ToList();
            var col = new ObservableCollection<Check>(checks);
            dgChecks.ItemsSource = null;
            dgChecks.ItemsSource = checks;
            dgChecks.Items.Refresh();
        }
    }
}
