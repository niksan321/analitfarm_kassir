﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace analitfarm_kassir
{
    /// <summary>
    /// Простое ограничение ввода
    /// Позволяет вводить в текстовое поле только цифры и точки
    /// </summary>
    public class DoubleTextBox : TextBox
    {
        protected override void OnTextInput(TextCompositionEventArgs e)
        {
            e.Handled = !IsDouble(e.Text);
            base.OnTextInput(e);
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            e.Handled = (e.Key == Key.Space);
            base.OnPreviewKeyDown(e);
        }

        private bool IsDouble(string str)
        {
            foreach (char c in str)
            {
                if (c.Equals('.'))
                    return true;

                else if (Char.IsNumber(c))
                    return true;
            }
            return false;
        }
    }
}
