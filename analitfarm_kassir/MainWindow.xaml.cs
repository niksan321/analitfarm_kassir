﻿using System.Windows;
using System.Windows.Controls;

namespace analitfarm_kassir
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// При заходе на страницу закрытых чеков, обновляем данные
        /// </summary>
        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                if (tiChecks.IsSelected)
                {
                    var checkPage = (ChecksPage)fChecks.Content;
                    checkPage.Refresh();
                }
            }

        }
    }
}
