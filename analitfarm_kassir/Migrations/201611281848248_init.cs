namespace analitfarm_kassir
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Checks",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Data = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                    Sum = c.Double(nullable: false),
                    Cash = c.Double(nullable: false),
                    Change = c.Double(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.CheckItems",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Quantity = c.Double(nullable: false),
                    Sum = c.Double(nullable: false),
                    Check_Id = c.Long(),
                    Product_Name = c.String(maxLength: 4000),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Checks", t => t.Check_Id)
                .ForeignKey("dbo.Products", t => t.Product_Name)
                .Index(t => t.Check_Id)
                .Index(t => t.Product_Name);

            CreateTable(
                "dbo.Products",
                c => new
                {
                    Name = c.String(nullable: false, maxLength: 4000),
                    Price = c.Double(nullable: false),
                    Rest = c.Double(nullable: false),
                })
                .PrimaryKey(t => t.Name);

        }

        public override void Down()
        {
            DropForeignKey("dbo.CheckItems", "Product_Name", "dbo.Products");
            DropForeignKey("dbo.CheckItems", "Check_Id", "dbo.Checks");
            DropIndex("dbo.CheckItems", new[] { "Product_Name" });
            DropIndex("dbo.CheckItems", new[] { "Check_Id" });
            DropTable("dbo.Products");
            DropTable("dbo.CheckItems");
            DropTable("dbo.Checks");
        }
    }
}
