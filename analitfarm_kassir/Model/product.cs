﻿using System.ComponentModel.DataAnnotations;

namespace analitfarm_kassir.Model
{
    /// <summary>
    /// Продукт
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Имя
        /// </summary>
        [Key]
        public string Name { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Остатки
        /// </summary>
        public double Rest { get; set; }
    }
}
