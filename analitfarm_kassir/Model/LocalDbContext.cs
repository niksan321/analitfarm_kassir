﻿using analitfarm_kassir.Model;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace analitfarm_kassir.model
{
    /// <summary>
    /// Контекст работы с бд
    /// </summary>
    public class LocalDbContext : DbContext
    {
        /// <summary>
        /// Продукты
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Чеки
        /// </summary>
        public DbSet<Check> Checks { get; set; }

        /// <summary>
        /// Позиции в чеке
        /// </summary>
        public DbSet<CheckItem> CheckItems { get; set; }

        public LocalDbContext()
        {
            Database.SetInitializer(new CreateNotifierDatabaseIfNotExists());
        }
    }

    public class CreateNotifierDatabaseIfNotExists : CreateDatabaseIfNotExists<LocalDbContext>
    {
        /// <summary>
        /// Демо данные
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(LocalDbContext context)
        {
            if (!context.Products.Any())
            {
                for (int i = 1; i <= 100; i++)
                {
                    context.Products.AddOrUpdate(
                        new Product
                        {
                            Name = $"Продукт номер {i}",
                            Price = i,
                            Rest = i
                        });
                }
                context.SaveChanges();
            }
        }
    }
}
