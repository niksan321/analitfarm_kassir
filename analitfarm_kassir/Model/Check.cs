﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace analitfarm_kassir.Model
{
    /// <summary>
    /// Чек
    /// </summary>
    public class Check : INotifyPropertyChanged
    {
        /// <summary>
        /// Номер чека
        /// </summary>
        [Key]
        public long Id { get; set; }

        DateTime data;
        /// <summary>
        /// Дата продажи (устанавливается автоматически при добавлении чека вбд)
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Data
        {
            get { return data; }
            set { data = value; Notify(); }
        }

        /// <summary>
        /// Товары и их количество
        /// </summary>
        public virtual ICollection<CheckItem> Items { get; set; }

        double sum;
        /// <summary>
        /// Сумма чека
        /// </summary>
        public double Sum
        {
            get { return sum; }
            set { sum = value; Notify(); }
        }

        double cash;
        /// <summary>
        /// Наличные
        /// </summary>
        public double Cash
        {
            get { return cash; }
            set { cash = value; Notify(); }
        }

        double change;
        /// <summary>
        /// Сдача
        /// </summary>
        public double Change
        {
            get { return change; }
            set { change = value; Notify(); }
        }

        public Check()
        {
            Items = new ObservableCollection<CheckItem>();
        }

        /// <summary>
        /// Пересчёт суммы и сдачи
        /// </summary>
        public void RefreshSumAndChange()
        {
            double sum = 0;
            foreach (var item in Items)
            {
                sum += item.Sum;
            }
            Sum = sum;

            if (Cash > Sum)
            {
                Change = Cash - Sum;
            }
            else
            {
                Change = 0;
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void Notify([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
