﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace analitfarm_kassir.Model
{
    /// <summary>
    /// Позиция в чеке
    /// </summary>
    public class CheckItem : INotifyPropertyChanged
    {
        [Key]
        public long Id { get; set; }
        
        /// <summary>
        /// Продукт
        /// </summary>
        public virtual Product Product { get; set; }

        /// <summary>
        /// Количество продукта
        /// </summary>
        double quantity;
        /// <summary>
        /// Количество продукта
        /// </summary>
        public double Quantity
        {
            get { return quantity; }
            set { quantity = value; Notify(); }
        }

        /// <summary>
        /// Сумма по текущей позиции
        /// </summary>
        double sum;
        /// <summary>
        /// Сумма по текущей позиции
        /// </summary>
        public double Sum {
            get { return sum; }
            set { sum = value; Notify(); }
        }

        /// <summary>
        /// Пересчёт суммы позиции в чеке
        /// </summary>
        public void RefreshSum()
        {
            Sum = Quantity * Product.Price;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void Notify([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}