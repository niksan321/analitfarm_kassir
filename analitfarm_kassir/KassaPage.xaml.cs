﻿using analitfarm_kassir.model;
using analitfarm_kassir.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace analitfarm_kassir
{
    /// <summary>
    /// Так как в условии задачи не было требований по корректной работе несколькольких копий приложения над одной бд,
    /// то для упрощения логики и 
    /// </summary>
    public partial class KassaPage : Page
    {
        /// <summary>
        /// Текущий чек пользователя
        /// </summary>
        Check currentCheck { get; set; }

        /// <summary>
        /// Окно для показа пользователю системных сообщений
        /// </summary>
        Message message { get; set; }

        public KassaPage()
        {
            InitializeComponent();
            RefreshDgProducts();
            NewCurrentCheck();
        }

        /// <summary>
        /// Закрытие чека
        /// </summary>
        private void bSale_Click(object sender, RoutedEventArgs e)
        {
            var db = new LocalDbContext();
            var connection = db.Database.Connection;
            connection.Open();
            using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
            {
                #region Проверка суммы чека
                if (!CheckSum())
                {
                    return;
                } 
                #endregion

                foreach (var item in currentCheck.Items)
                {
                    #region Обновление данных о продукте из бд и проверка остатков
                    if (!ReloadProduct(item.Product, db))
                    {
                        return;
                    }
                    if (!CheckRests(item, db))
                    {
                        return;
                    } 
                    #endregion
                    item.Product.Rest -= item.Quantity;
                }

                db.Checks.Add(currentCheck);
                db.SaveChanges();
                transaction.Commit();

                #region сообщение пользователю что всё ок
                message.Show
                       (
                          Message.eMessageType.info,
                          $"Чек успешно закрыт, номер чека {currentCheck.Id}"
                       ); 
                #endregion

                RefreshDgProducts();
                NewCurrentCheck();
            }
        }

        /// <summary>
        /// Обновляет номенклатуру из бд
        /// и если её там нет, показывает пользователю сообщение об её отсутствии
        /// (такое может произойти, например после добавления в чек номенклатуры одним пользователем, 
        /// в то время как другой пользователь удалил её из бд)
        /// </summary>
        /// <param name="product">проверяемая номенклатура</param>
        /// <param name="db">контекст бд</param>
        /// <returns>true - номенклатура есть в бд, иначе - false</returns>
        private bool ReloadProduct(Product product, LocalDbContext db)
        {
            var name = product.Name;
            try
            {
                db.Products.Attach(product);
                db.Entry(product).Reload();
                return true;
            }
            catch (Exception ex)
            {
                var s = ex;
            }
            message.Show
                (
                    Message.eMessageType.error,
                    $"В базе данных отсутствует номенклатура с именем {name}"
                );
            return false;
        }

        /// <summary>
        /// Проверка у текущего чека суммы и наличных
        /// Если количество наличных меньше чем сумма чека, 
        /// то пользователю выводится сообщение об ошибке
        /// </summary>
        /// <returns>true - сумма чека меньше чем передано наличных, иначе - false</returns>
        private bool CheckSum()
        {
            if (currentCheck.Sum > currentCheck.Cash)
            {
                message.Show
                    (
                        Message.eMessageType.error,
                        $"Не достаточно наличных (сумма чека больше чем количество введённых наличных денег)"
                    );
                return false;
            }
            if (currentCheck.Sum == 0)
            {
                message.Show
                    (
                        Message.eMessageType.error,
                        $"Не допускается закрывать чеки с нулевой суммой"
                    );
                return false;
            }
            return true;
        }

        /// <summary>
        /// Проверка остатков позиции в чеке,
        /// если остатков не хватает, пользователю показывается сообщение
        /// </summary>
        /// <param name="item">товарная позиция в чеке</param>
        /// <param name="db">контекст бд</param>
        /// <returns>true - товара есть на остатке, иначе - false</returns>
        private bool CheckRests(CheckItem item, LocalDbContext db)
        {
            if (item.Product.Rest < item.Quantity)
            {
                message.Show
                    (
                        Message.eMessageType.error,
                        $"Не достаточно остатков у товара: {item.Product.Name}"
                    );
                RefreshDgProducts();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Создаём новы текущий чек
        /// </summary>
        private void NewCurrentCheck()
        {
            currentCheck = new Check();
            dgCheck.ItemsSource = currentCheck.Items;
            tbCash.DataContext = currentCheck;
            lSum.DataContext = currentCheck;
            lChange.DataContext = currentCheck;
            dgCheck.CellEditEnding += dgCheck_CellEditEnding;
        }

        /// <summary>
        /// чтобы редактирование количества работало не только по клавише Enter
        /// а также чтобы автоматически пересчитывать сумму и сдачу текущего чека
        /// отлавливаем соытие редактирования количества товара в чеке
        /// </summary>
        void dgCheck_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                var column = e.Column as DataGridBoundColumn;
                if (column != null)
                {
                    var bindingPath = (column.Binding as Binding).Path.Path;
                    if (bindingPath == "Quantity")
                    {
                        var el = (TextBox)e.EditingElement;
                        var item = (CheckItem)dgCheck.Items[e.Row.GetIndex()];
                        double quantity = 0;
                        double.TryParse(el.Text, out quantity);
                        item.Quantity = quantity;
                        item.RefreshSum();
                        currentCheck.RefreshSumAndChange();
                    }
                }
            }
        }

        /// <summary>
        /// При изменении наличных в чеке, пересчитываем сдачу
        /// </summary>
        private void tbCash_TextChanged(object sender, TextChangedEventArgs e)
        {
            currentCheck.RefreshSumAndChange();
        }

        /// <summary>
        /// Обновление списка номенклатуры, с учётом строки поиска
        /// </summary>
        private void RefreshDgProducts()
        {
            string searchName = tbSearch.Text.Trim();
            using (var db = new LocalDbContext())
            {
                var dbProducts = db.Products.AsQueryable();
                if (!string.IsNullOrWhiteSpace(searchName))
                {
                    dbProducts = db.Products.Where(x => x.Name.Contains(searchName));
                }
                dgProducts.ItemsSource = dbProducts.OrderBy(x => x.Name).ToList();
            }
        }

        /// <summary>
        /// Добавление выбранного товара в чек по двойному клику
        /// </summary>
        private void dgProducts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dataGreed = (DataGrid)sender;
            var selectedProduct = (Product)dataGreed?.SelectedItem;
            if (selectedProduct != null)
            {
                if (selectedProduct.Rest > 0)
                {
                    if (!currentCheck.Items.Any(x => x.Product.Name == selectedProduct.Name))
                    {
                        var checkItem = new CheckItem
                        {
                            Product = selectedProduct,
                            Quantity = 1
                        };
                        checkItem.RefreshSum();
                        currentCheck.Items.Add(checkItem);
                        currentCheck.RefreshSumAndChange();
                    }
                }
                else
                {
                    message.Show
                       (
                          Message.eMessageType.info,
                          $"У выбранного товара не хватает остатков"
                       );
                }
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var navigationService = NavigationService.GetNavigationService(this);
            message = new Message(navigationService, this);
        }

        /// <summary>
        /// Живой поиск по номенклатуре
        /// </summary>
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefreshDgProducts();
        }
    }
}
