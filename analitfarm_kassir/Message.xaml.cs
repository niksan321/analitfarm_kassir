﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace analitfarm_kassir
{
    /// <summary>
    /// Сообщение для пользователя
    /// </summary>
    public partial class Message : Page
    {
        /// <summary>
        /// Тип сообщения
        /// </summary>
        public enum eMessageType
        {
            /// <summary>
            /// Обычное
            /// </summary>
            info,

            /// <summary>
            /// Ошибка
            /// </summary>
            error
        }

        /// <summary>
        /// сервис навигации по страницам
        /// </summary>
        NavigationService rootNavigationService;
        
        /// <summary>
        /// Страница возврата
        /// </summary>
        Page backPage;

        /// <summary>
        /// Создание сообщение для пользователя
        /// </summary>
        /// <param name="navService">сервис навигации</param>
        /// <param name="backPage">страница возврата</param>
        public Message(NavigationService navService, Page backPage)
        {
            rootNavigationService = navService;
            this.backPage = backPage;
            InitializeComponent();
        }

        /// <summary>
        /// Показать сообщение пользователю
        /// </summary>
        /// <param name="type">тип сообщения</param>
        /// <param name="text">текст сообщения</param>
        public void Show(eMessageType type, string text)
        {
            switch (type)
            {
                case eMessageType.info:
                    LCaption.Background = new SolidColorBrush(Color.FromRgb(50, 50, 200));
                    LCaption.Content = "Информация";
                    break;
                case eMessageType.error:
                    LCaption.Background = new SolidColorBrush(Color.FromRgb(200, 50, 50));
                    LCaption.Content = "Ошибка";
                    break;
                default:
                    throw new NotImplementedException();
            }
            tbMessage.Text = text;
            rootNavigationService.Navigate(this);
        }

        /// <summary>
        /// Закрытие сообщения
        /// </summary>
        private void bOk_Click(object sender, RoutedEventArgs e)
        {
            rootNavigationService.Navigate(backPage);
            ClearHistory(rootNavigationService);
        }

        /// <summary>
        /// Очистка истории навигации
        /// </summary>
        /// <param name="nav"></param>
        public void ClearHistory(NavigationService nav)
        {
            if (!nav.CanGoBack && !nav.CanGoForward)
            {
                return;
            }

            var entry = nav.RemoveBackEntry();
            while (entry != null)
            {
                entry = nav.RemoveBackEntry();
            }
        }
    }
}
